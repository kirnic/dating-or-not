import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable, of, interval } from 'rxjs';
import { mapTo, tap, delay, take } from 'rxjs/operators';

@Component({
  selector: 'app-result-view',
  templateUrl: './result-view.component.html',
  styleUrls: ['./result-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultViewComponent implements OnInit {
  states = [
    '15% сбор анализов...',
    '30% подключение искусственного интеллекта',
    '45% применение машинного обучения',
    '60% подключение карликов',
    '75% сверка с гороскопом...',
    '90% сверка с фазами луны...',
    '100%',
    'готово'
  ];

  spinnerPath = 'assets/spinner-heart.svg';
  fireWorkPath = 'assets/firework3.gif';

  constructor() { }

  ngOnInit() { }
}
