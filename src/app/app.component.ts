import { Component, OnInit } from '@angular/core';
import {Howl, Howler} from 'howler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  sound = new Howl({
    src: ['assets/background.mp3']
  });

  ngOnInit() {
    this.sound.play();
    Howler.volume(0.5);
  }
}
