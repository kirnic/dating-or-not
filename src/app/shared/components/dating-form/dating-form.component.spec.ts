import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatingFormComponent } from './dating-form.component';

describe('DatingFormComponent', () => {
  let component: DatingFormComponent;
  let fixture: ComponentFixture<DatingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
