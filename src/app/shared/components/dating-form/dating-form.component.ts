import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-dating-form',
  templateUrl: './dating-form.component.html',
  styleUrls: ['./dating-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatingFormComponent implements OnInit {
  @Output()
  activationClick: EventEmitter<any> = new EventEmitter();

  states = [
    '25% проверка звука...',
    '50% ловля бабочек...',
    '75% поимка карликов...',
    '90% распыление гармонов',
    '100%',
    'готово'
  ];

  spinnerPath = 'assets/spinner-infinity.svg';


  girlFormGroup: FormGroup;
  boyFormGroup: FormGroup;
  likeFormGroup: FormGroup;

  programStars = 3;
  partnerStars = 5;

  ratingIcons = {
    empty: 'assets/femenine.svg',
    half: 'assets/femenine.svg',
    full: 'assets/masculine.svg',
  };

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.girlFormGroup = this.formBuilder.group({
      nameCtrl: ['', Validators.required]
    });
    this.boyFormGroup = this.formBuilder.group({
      nameCtrl: ['',
        [
          Validators.required,
          Validators.pattern('Кирилл')
        ]
      ]
    });

    this.likeFormGroup = this.formBuilder.group({
      likeRadioCtrl: [null,
        [
          Validators.required
        ]
      ]
    });
  }

  get valid() {
    return !![
      this.girlFormGroup,
      this.boyFormGroup,
      this.likeFormGroup,
      { valid: this.programStars === 5 }
    ].every(group => group.valid);
  }

  onCultureRatingSet(stars: any) {
    this.programStars = stars;
  }

  onPartnerRatingSet(stars: any) {
    this.partnerStars = 5;
  }

  onActivationClick(e: any) {
    this.activationClick.emit();
  }
}
