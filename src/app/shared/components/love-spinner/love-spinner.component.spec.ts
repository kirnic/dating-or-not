import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoveSpinnerComponent } from './love-spinner.component';

describe('LoveSpinnerComponent', () => {
  let component: LoveSpinnerComponent;
  let fixture: ComponentFixture<LoveSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoveSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoveSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
