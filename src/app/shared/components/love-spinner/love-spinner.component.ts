import { ChangeDetectionStrategy, Component, OnInit, Input } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { concatMap, delay, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-love-spinner',
  templateUrl: './love-spinner.component.html',
  styleUrls: ['./love-spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoveSpinnerComponent implements OnInit {
  @Input()
  states: string[] = [];

  @Input()
  spinnerPath: string[] = [];

  state: Observable<string>;
  visible = true;

  *generateState() {
    for (const state of this.states) {
      yield state;
    }

    return;
  }

  constructor() { }

  ngOnInit() {
    this.state = from(this.generateState()).pipe(
      concatMap(item => of(item).pipe(
        delay(2500)
      )),
      finalize(() => this.visible = false)
    );
  }
}
