import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoveSpinnerComponent } from './components/love-spinner/love-spinner.component';
import { LoadingSpinnersModule } from 'ngx-loading-spinners';
import { DatingFormComponent } from './components/dating-form/dating-form.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxStarsModule } from 'ngx-stars';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { RouterModule } from '@angular/router';
import { DxPieChartModule } from 'devextreme-angular';

@NgModule({
  declarations: [
    LoveSpinnerComponent,
    DatingFormComponent
  ],
  exports: [
    LoveSpinnerComponent,
    DatingFormComponent,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatRadioModule,
    ReactiveFormsModule,
    NgxStarsModule,
    ScrollingModule,
    MatToolbarModule,
    DxPieChartModule
  ],
  imports: [
    CommonModule,
    LoadingSpinnersModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatRadioModule,
    ReactiveFormsModule,
    NgxStarsModule,
    ScrollingModule,
    RouterModule,
    MatToolbarModule,
    DxPieChartModule
  ]
})
export class SharedModule { }
