import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultViewComponent } from 'src/app/layouts/result-view/result-view.component';
import { DatingFormComponent } from './shared/components/dating-form/dating-form.component';
import { ContactsComponent } from './containers/contacts/contacts.component';
import { MainComponent } from './layouts/main/main.component';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'dating', component: DatingFormComponent },
  { path: 'results', component: ResultViewComponent },
  { path: 'contacts', component: ContactsComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
